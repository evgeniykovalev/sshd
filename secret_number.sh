#!/bin/bash

secret_number=$((RANDOM % 100 + 1))

max_attempts=5
attempts=0

echo "Число от 1 до 100"

	while [ $attempts -lt $max_attempts ]; do
		read -p "Ваше число? " guess
		
		if [ $guess -eq $secret_number ]; then
			echo "Ви вгадали!"
			exit 0
		elif [ $guess -lt $secret_number ]; then
			echo "Занадто низько"
		else
			echo "Занадто високо"
		fi

		((attempts++))
	done

	echo "Ви вичерпали всі спроби. Число $secret_number  було вірним"
